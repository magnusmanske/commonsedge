<?PHP

require_once ( '/data/project/commonsedge/commonsedge.php' ) ;
require_once ( '/data/project/commonsedge/public_html/php/ToolforgeCommon.php' ) ;
require_once ( '/data/project/commonsedge/public_html/php/wikidata.php' ) ;
require_once ( '/data/project/quickstatements/public_html/quickstatements.php' ) ;

class CommonsEdgeBase {

	protected $filename ;
	protected $item ; # item ID
	protected $i ; # WIL item
	protected $fd ; # parsed file description

	protected $wd2local ;
	protected $ce ;
	protected $qs ;
	protected $wil ;
	protected $property_mappings = [
		'text_note' => [ 'p' => 'P77782' ] ,
		'license' => [ 'p' => 'P77772' ] ,
		'extracted_at' => [ 'p' => 'P77773' ] ,
		'author_commons_user' => [ 'p' => 'P77775' ] ,
		'author_wikidata_item' => [ 'p' => 'P77776' ] ,
		'author_plain_text' => [ 'p' => 'P77777' ] ,
		'uploader' => [ 'p' => 'P77779' ] ,
		'file_on_commons' => [ 'p' => 'P77778' ] ,
		'category' => [ 'p' => 'P77780' ] ,
		'flickr_file_id' => [ 'p' => 'P77785' ] ,
		'coordinates' => [ 'p' => 'P77781' ] ,
		'tag_template' => [ 'p' => 'P77790' ] ,
		'source_url' => [ 'p' => 'P77783' ] ,
		'reviewer' => [ 'p' => 'P77786' ] ,
		'review_date' => [ 'p' => 'P77787' ] ,
		'source_text' => [ 'p' => 'P77784' ] ,
		'creation_date' => [ 'p' => 'P77774' ]
	] ;
	protected $external_id_templates = [
		'Rijksmonument' => 'P77788' ,
		'Extracted from' => 'P77789'
	] ;

	function __construct ( $filename , $item = '' ) {
		global $wikidata_api_url ;
		$this->wd2local = json_decode ( file_get_contents ('/data/project/commonsedge/wd2local.json') ) ; # HACK mapping of license items etc
		$this->filename = $filename ;
		if ( $item != '' ) $this->item = $item ;
		$this->ce = new CommonsEdge() ;
		$this->ce->verbose = true ;
		$this->tfc = new ToolforgeCommon('commonsedge') ;
		$this->qs = $this->tfc->getQS('CommonsEdge test','/data/project/commonsedge/bot.ini',true) ;
		$this->qs->config->site = 'wikidata-test' ;
		$wikidata_api_url = $this->qs->getAPI() ;
		$this->wil = new WikidataItemList() ;
	}

	public function getFilename () {
		return $this->filename ;
	}

	public function parseFileDescription () {
		$this->fd = $this->ce->parseFileDescription ( $this->filename ) ;
		return $this->fd ;
	}

	public function getDataItem () {
		if ( isset($this->item) ) return $this->item ;
		# TODO get item if exists
		return $item ;
	}

	public function sanitizeDescriptionString ( $s ) {
		$s = str_replace ( "_" , ' ' , $s ) ;
		$s = str_replace ( "\t" , ' ' , $s ) ;
		$s = str_replace ( "\n" , ' ' , $s ) ;
		$s = substr ( $s , 0 , 240 ) ; # 250 instead of 250, there seem to be some unicode issues
		return $s ;
	}

	public function getOrCreateDataItem () {
		$item = $this->getDataItem() ;
		if ( isset($item) ) return $item ;
		$label = $this->sanitizeDescriptionString ( preg_replace ( '/\.[a-z]+$/i' , '' , $this->filename ) ) ;

		$this->item = 'LAST' ;
		$this->i = new WDI() ;
		$commands = $this->generateUpdateCommands() ;
		array_unshift ( $commands , "LAST\tLen\t\"{$label}\"" ) ;
		array_unshift ( $commands , "CREATE" ) ;
#print_r ( $commands ) ; exit(0) ;
		$item = $this->tfc->runCommandsQS ( $commands ) ;
		$this->item = $item ;
		unset ( $this->i ) ; # Out-of-date
		$this->wil->updateItems ( [$item] ) ; # Force refresh
		return $item ;
	}

	protected function loadItem () {
		if ( isset($this->i) ) return ; // Already loaded, we hope
		if ( !isset($this->item) ) die ( "CommonsEdgeBase::loadItem - no item set\n" ) ;
		$this->wil->loadItem ( $this->item ) ;
		$this->i = $this->wil->getItem ( $this->item ) ;
		if ( !isset($this->i) ) die ( "CommonsEdgeBase::loadItem - can't load item {$this->item}\n" ) ;
	}

	# This is used for the TEST WIKIDATA SITE, to map Wikidata items to local dummy equivalents, as test site doesn't seem to have federation
	protected function wd2localQ ( $q ) {
		if ( isset($this->wd2local->$q) ) return $this->wd2local->$q ;
		return $q ; # Huh
	}

	protected function sanitizeTime ( $time ) { # HARD HACK - wikibase doesn't work with seconds?
		$time = preg_replace ( '|T\d\d:\d\d:\d\dZ/14|' , 'T00:00:00Z/11' , $time ) ;
		return $time ;
	}

	protected function ensureLicenceItemExists ( $q ) {
		if ( isset($this->wd2local->$q) ) return $this->wd2local->$q ;

		# THE FOLLOWING IS A HACK TO CREATE AN EQUIVALENT LICENSE ITEM ON THE TEST SITE
		$url = "https://www.wikidata.org/w/api.php?action=wbgetentities&format=json&ids={$q}" ;
		$j = json_decode ( file_get_contents ( $url ) ) ;
		$i = new WDI () ;
		$i->q = $q ;
		$i->j = $j->entities->$q ;
		$label = $i->getLabel() ;
		$label = preg_replace ( '/^Template:/' , '' , $label ) ;
		$commands = [
			'CREATE' ,
			"LAST\tLen\t\"$label\"" ,
			"LAST\tP82\tQ168800"
		] ;
		$new_q = $this->tfc->runCommandsQS ( $commands ) ;
		$this->wd2local->$q = $new_q ;
		file_put_contents ( '/data/project/commonsedge/wd2local.json' , json_encode($this->wd2local) ) ;
		return $new_q ;
	}

	public function generateUpdateCommands () {
		$this->loadItem() ;
		$i = $this->i ; # Item structure
		if ( !isset($this->fd) ) $this->parseFileDescription() ;
		if ( $this->fd === false ) die ( "CommonsEdgeBase::syncItem - can't parse file {$this->filename}\n" ) ;
		$fd = $this->fd ;

		$ts = '+' . date('Y-m-d') . 'T00:00:00Z/11' ;
		$quals = "{$this->property_mappings['extracted_at']['p']}\t{$ts}" ;
		$p_notes = $this->property_mappings['text_note']['p'] ;

		$commands = [] ;

		$p = $this->property_mappings['file_on_commons']['p'] ;
		if ( !$i->hasClaims($p) ) {
			$commands[] = "{$this->item}\t{$p}\t\"{$this->filename}\"" ;
		}

		# Descriptions
		# TODO description property?
		foreach ( $fd['description'] AS $d ) {
			$lang = preg_replace ( '/^monolingual:/' , '' , $d[0] ) ;
			if ( $i->hasDescriptionInLanguage($lang) ) continue ;
			$desc = $this->sanitizeDescriptionString ( $d[1] ) ;
			$desc = preg_replace ( '|^"*(.+)"\s+"text"$|' , '$1' , $desc ) ; # Hack around some upstream bug
			$commands[] = "{$this->item}\tD{$lang}\t\"{$desc}\"" ;
		}

		# Licenses
		# TODO skip if ANY license present?
		$p = $this->property_mappings['license']['p'] ;
		foreach ( $fd['license'] AS $l ) {
			if ( $l[0] != 'url' or !preg_match ( '|^https*://www.wikidata.org/wiki/(Q\d+)$|' , $l[1] , $m ) ) continue ;
			$q = $this->ensureLicenceItemExists ( $m[1] ) ;
#			$q = $this->wd2localQ ( $m[1] ) ;
			if ( $i->hasTarget ( $p , $q ) ) continue ; # Already in item
			$commands[] = "{$this->item}\t{$p}\t{$q}\t{$quals}" ;
		}

		# Dates
		$p = $this->property_mappings['creation_date']['p'] ;
		foreach ( $fd['date'] AS $d ) {
			if ( $i->hasClaims($p) ) continue ; # Skipping if ANY file creation time/date present
			if ( $d[0] != 'time' ) continue ;
			$time = $this->sanitizeTime ( $d[1] ) ;
			$commands[] = "{$this->item}\t{$p}\t{$time}\t{$quals}" ;
		}

		# Authors
		foreach ( $fd['author'] AS $a ) {
			$text = $a[1] ;
			$p = '' ;
			if ( $a[0] == 'url' ) {
				if ( preg_match ( '|^https*://commons.wikimedia.org/wiki/User:(.+)$|' , $text , $m ) ) {
					$p = $this->property_mappings['author_commons_user']['p'] ;
					$text = $m[1] ;
				} else if ( preg_match ( '|^https*://www.wikidata.org/wiki/(Q\d+)$|' , $text , $m ) ) {
					$p = $this->property_mappings['author_wikidata_item']['p'] ;
					$text = $m[1] ;
				}
			}
			if ( $p == '' ) { // Fallback
				$text = $this->sanitizeDescriptionString ( $text ) ;
				$p = $this->property_mappings['author_plain_text']['p'] ;
			}
			if ( $i->hasClaims ( $p ) ) continue ; # Already a statement with that property in item
			$commands[] = "{$this->item}\t{$p}\t\"{$text}\"\t{$quals}" ;
		}

		# Uploaders
		$p = $this->property_mappings['uploader']['p'] ;
		foreach ( $fd['uploader'] AS $u ) {
			if ( $u[0] != 'url' ) continue ;
			if ( $i->hasClaims ( $p ) ) continue ; # Already a statement with that property in item
			if ( !preg_match ( '|^https*://commons.wikimedia.org/wiki/User:(.+)$|' , $u[1] , $m ) ) continue ;
			$user = $m[1] ;
			$commands[] = "{$this->item}\t{$p}\t\"{$user}\"\t{$quals}" ;
		}

		# Categories
		# TODO check individual categories?
		$p = $this->property_mappings['category']['p'] ;
		foreach ( $fd['categories'] AS $c ) {
			if ( $i->hasClaims ( $p ) ) continue ; # Already a statement with that property in item
			$commands[] = "{$this->item}\t{$p}\t\"{$c}\"\t{$quals}" ;
		}

		# location
		$p = $this->property_mappings['coordinates']['p'] ;
		foreach ( $fd['location'] AS $l ) {
			if ( $l[0] != 'coordinate' ) continue ;
			if ( $i->hasClaims ( $p ) ) continue ; # Already a statement with that property in item
			$lat = $l[1]['latitude'] ;
			$lon = $l[1]['longitude'] ;
			$notes = $l[1]['attributes'] ;
			$cmd = "{$this->item}\t{$p}\t@{$lat}/{$lon}" ;
			if ( isset($notes) ) $cmd .= "\t{$p_notes}\t\"" . $this->sanitizeDescriptionString($notes) . "\"" ;
			$cmd .= "\t{$quals}" ;
			$commands[] = $cmd ;
		}

		# source
		# TODO skip if either prop exists
		foreach ( $fd['source'] AS $s ) {
			$p = $this->property_mappings[$s[0]=='url'?'source_url':'source_text']['p'] ;
			if ( $i->hasClaims ( $p ) ) continue ; # Already a statement with that property in item
			$text = $s[1] ;
			if ( $s[0]=='url' ) {
				if ( preg_match ( '|^https*://w*\.*flickr\.com/photos/[^/]+/(\d+)/*$|' , $text , $m ) ) {
					$commands[] = "{$this->item}\t{$this->property_mappings['flickr_file_id']['p']}\t\"{$m[1]}\"\t{$quals}" ;
				}
				if ( strlen($text) > 245 ) { # Long URL fallback
					$p = $this->property_mappings['source_text']['p'] ;
					$text = $this->sanitizeDescriptionString ( $text ) ;
				}
			} else {
				$text = $this->sanitizeDescriptionString ( $text ) ;
			}
			$commands[] = "{$this->item}\t{$p}\t\"{$text}\"\t{$quals}" ;
		}
		
		# review
		$p = $this->property_mappings['reviewer']['p'] ;
		$p2 = $this->property_mappings['review_date']['p'] ;
		$p3 = $this->property_mappings['license']['p'] ;
		foreach ( $fd['review'] AS $r ) {
			if ( $i->hasClaims ( $p ) ) continue ; # Already a statement with that property in item
			if ( $r['reviewer'][0] != 'url' or !preg_match ( '|^https*://commons.wikimedia.org/wiki/User:(.+)$|' , $r['reviewer'][1] , $m ) ) continue ;
			$user = $m[1] ;
			$cmd = "{$this->item}\t{$p}\t\"{$user}\"" ;
			if ( isset($r['date']) and $r['date'][0] == 'time' ) $cmd .= "\t{$p2}\t" . $this->sanitizeTime($r['date'][1]) ;
			if ( isset($r['license']) and $r['license'][0] == 'url' and preg_match ( '|^https*://www.wikidata.org/wiki/(Q\d+)$|' , $r['license'][1] , $m ) ) {
				$q = $this->ensureLicenceItemExists ( $m[1] ) ;
				$cmd .= "\t{$p3}\t{$q}" ;
			}
			$cmd .= "\t{$quals}" ;
			$commands[] = $cmd ;
		}

		# templates
		foreach ( $fd['template'] AS $t ) {
			if ( count($t['parameters']) != 1 ) continue ;
			foreach ( $this->external_id_templates AS $tn => $tp ) {
				if ( $tn != $t['name'] ) continue ;
				$id = $this->sanitizeDescriptionString ( array_shift ( $t['parameters'] ) ) ;
				if ( $i->hasClaims ( $tp ) ) continue ; # Already a statement with that property in item
				$commands[] = "{$this->item}\t{$tp}\t\"{$id}\"\t{$quals}" ;
				break ;
			}
		}

		# tag_template
		$p = $this->property_mappings['tag_template']['p'] ;
		foreach ( $fd['tag_template'] AS $t ) {
			if ( $t[0] != 'text' ) continue ;
			if ( $i->hasClaims ( $p ) ) continue ; # Already a statement with that property in item
			$text = $this->sanitizeDescriptionString ( $t[1] ) ;
			$commands[] = "{$this->item}\t{$p}\t\"{$text}\"\t{$quals}" ;
		}

		return array_unique ( $commands ) ;
	}

	public function syncItem () {
		$commands = $this->generateUpdateCommands() ;
		print_r ( $commands ) ;
		$this->tfc->runCommandsQS ( $commands ) ;
	}

}

?>
