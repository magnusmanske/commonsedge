# README #

* This is a PHP parser for *some* file description pages on Wikimedia Commons
* It generates simple JSON output that could be used as a basis for the upcoming Commons-Wikibase installation
* There is [an API](https://tools.wmflabs.org/commonsedge/api.php) to generate the JSON for individual files
* It is designed to *either* create [a result](https://tools.wmflabs.org/commonsedge/api.php?file=%C3%9Adol%C3%AD+Hasiny+u+Lipence+2015-10-03+Hasina+2.jpg) that encompasses all information on the file description page, *or* [fail](https://tools.wmflabs.org/commonsedge/api.php?file=%D0%9A%D0%BE%D0%BC%D0%BF%D0%BB%D0%B5%D0%BA%D1%81+%D0%9C%D0%B8%D1%80%D1%81%D0%BA%D0%BE%D0%B3%D0%BE+%D0%B7%D0%B0%D0%BC%D0%BA%D0%B0.JPG) for complex pages
* Main class is in commonsedge.php