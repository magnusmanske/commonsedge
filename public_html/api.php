<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

set_time_limit ( 60 * 10 ) ; // 10 min

require_once ( 'php/common.php' ) ;
require_once ( '/data/project/commonsedge/commonsedge.php' ) ;

function getRandomFilename () {
	$j = json_decode ( file_get_contents ( 'https://commons.wikimedia.org/w/api.php?action=query&list=random&rnnamespace=6&format=json') ) ;
	$file = $j->query->random[0]->title ;
	$file = preg_replace ( '/^File:/' , '' , $file ) ;
	return $file ;
}

$file = get_request ( 'file' , '' ) ;
$random = isset($_REQUEST['random']) ;
$num = get_request ( 'num' , '1' ) ;

if ( $file == '' and !$random ) {
	print get_common_header ( '' , 'Commonsedge' ) ;
	print "<form method='get' action='?' class='form form-inline'>
	<h3>Commons file to get a machine-readable description for</h3>
	<input type='text' class='form-element' name='file' />
	<input type='submit' class='btn btn-primary' value='Get JSON' />
	</form>" ;
	
	print "<hr/><form method='get' action='?' class='form form-inline'>
	<h3>Check one or several random files</h3>
	Number of files: <input type='number' class='form-element' name='num' value='10' />
	<input type='submit' class='btn btn-primary' value='Check files' /> (1 will show the JSON, >1 a statistic/list)
	<input type='hidden' name='random' value='1' />
	</form>" ;

	exit ( 0 ) ;
}



if ( $random and $num > 1 ) {
	print get_common_header ( '' , 'Commonsedge' ) ;
	print "<h3>Checking $num random file descriptions</h3>" ;
	$bad = array() ;
	$ok = array() ;
	$ce = new CommonsEdge() ;
	for ( $i = 0 ; $i < $num ; $i++ ) {
		$file = getRandomFilename() ;
		$j = $ce->parseFileDescription ( $file ) ;
		if ( $j === false ) $bad[] = $file ;
		else $ok[] = $file ;
	}
	print "<p>OK: ".count($ok)."</p>" ;
	print "<p>N/A: ".count($bad)."</p>" ;
	printf ( "<p>Rate: %2.2f%% OK</p>" , 100*count($ok)/(count($ok)+count($bad)) ) ;
	print "<h3>File descriptions that can not be fully parsed at the moment</h3><ol>" ;
	foreach ( $bad AS $b ) {
		print "<li><a href='?file=".urlencode($b)."' target='_blank'>$b</a>" ;
		print " (<a href='//commons.wikimedia.org/wiki/File:".myurlencode($b)."' target='_blank'>Commons</a>)</li>" ;
	}
	print "</ol>" ;
	exit(0);
}


header('Content-type: text/plain; charset=UTF-8');

if ( $random ) $file = getRandomFilename() ;

$out = array ( 'status'=>'OK' , 'file' => $file , 'result' => (object) array() ) ;

$ce = new CommonsEdge() ;
$j = $ce->parseFileDescription ( $file ) ;
if ( $j === false ) {
	$out['status'] = 'ERROR' ;
	$out['error'] = $ce->error_text ;
	$out['error_data'] = $ce->error_data ;
} else {
	$out['result'] = $j ;
}


// Output
if ( isset($_REQUEST['callback']) ) print $_REQUEST['callback'].'(' ;
print json_encode ( $out , JSON_PRETTY_PRINT ) ;
if ( isset($_REQUEST['callback']) ) print ')' ;

?>