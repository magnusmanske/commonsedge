<?PHP

/* Test files:
https://commons.wikimedia.org/wiki/Commons:Structured_data/Get_involved/Feedback_requests/Interesting_Commons_files
*/

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // |E_ALL

require_once ( '/data/project/commonsedge/public_html/php/common.php' ) ;

class CommonsEdge {

	public $db ;
	public $parser_version ;
	public $error , $error_text , $error_data ;
	public $verbose = false ;
	
	function __construct() {
		$this->error = false ;
		$this->parser_version = 1 ;
	}
	
	function __destruct() {
		if ( isset ( $this->db ) ) $this->db->close() ;
	}

	protected function resetError () {
		$this->error = false ;
		$this->error_text = '' ;
		$this->error_data = array() ;
	}
	
	protected function setError ( $text , $data ) {
		$this->error = true ;
		$this->error_text = $text ;
		$this->error_data = $data ;
		if ( $this->verbose ) {
			print "ERROR: {$text}\n" ;
			print_r ( $data ) ;
		}
		return false ;
	}

	protected function month2num ( $m ) {
		$month2number = array (
			'january' => 1 ,
			'february' => 2 ,
			'march' => 3 ,
			'april' => 4 ,
			'may' => 5 ,
			'june' => 6 ,
			'july' => 7 ,
			'august' => 8 ,
			'september' => 9 ,
			'october' => 10 ,
			'november' => 11 ,
			'december' => 12
		) ;
		$ml = strtolower ( trim ( $m ) ) ;
		if ( !isset($month2number[$ml]) ) return $m ; // Skip
		$ret = $month2number[$ml] ;
		$ret = str_pad ( $ret , 2 , '0' ) ;
		return $ret ;
	}
	
	protected $url_cache = array() ;
	
	protected function getURL ( $url ) {
		if ( !isset($this->url_cache[$url]) ) $this->url_cache[$url] = file_get_contents ( $url ) ;
		return $this->url_cache[$url] ;
	}


	// MAIN METHODS
	
	protected $wikiRules = array (
		'\{\{' => 'template_open' ,
		'\}\}' => 'template_close' , 
		'\[\[' => 'link_open' , 
		'\]\]' => 'link_close' ,
		'\|' => 'pipe' ,
		'=' => 'eq' ,
		"\n" => 'newline' ,
		'.' => 'text'
	) ;
	
	protected function lexer ( $text , $rules ) {
		$ret = array() ;
		$l = 0 ;
		while ( $text != '' ) {
			foreach ( $rules AS $k => $v ) {
				if ( !preg_match ( '/^('.$k.')/' , $text , $m ) ) continue ;
				if ( count($ret) > 0 && $ret[count($ret)-1][1] == 'text' && $v == 'text' ) {
					$ret[count($ret)-1][0] .= $m[1] ;
				} else if ( $v == 'text' and trim($m[1]) == '' ) {
//				} else if ( $v == 'text' and preg_match ( '/^\s*<!--.+-->\s*$/' , $text ) ) {
				} else {
					$ret[] = array ( $m[1] , $v ) ;
				}
				$text = substr ( $text , strlen($m[1]) ) ;
				break ;
			}
			if ( $l == count($ret) ) {
				return $this->setError ( "Parse fail!" , $ret ) ;
			}
		}
		return $ret ;
	}
	
	protected function subgroupParts ( &$parts , $start , $end ) {
		for ( $i = 0 ; $i < count($parts) ; $i++ ) {
			if ( $parts[$i][0] != $start ) {
				if ( isset($parts[$i][2]) ) $this->subgroupParts ( $parts[$i][2] , $start , $end ) ; // Recursive
				continue ;
			}
			$cnt = 1 ;
			$tmp = array() ;
			for ( $j = $i+1 ; $j < count($parts) and $cnt>0 ; $j++ ) {
				if ( $parts[$j][0] == $start ) $cnt++ ;
				else if ( $parts[$j][0] == $end ) $cnt-- ;
			}
			$parts[$i][2] = array_splice ( $parts , $i+1 , $j-$i-1 ) ;
			array_pop ( $parts[$i][2] ) ; // STOP element
			$this->subgroupParts ( $parts[$i][2] , $start , $end ) ; // Recursive
		}
	}
	
	protected function shiftNewlines ( &$p ) {
		while ( count($p) > 0 and $p[0][1] == 'newline' ) array_shift ( $p ) ;
	}
	
	protected function parseTemplate ( &$p ) {
		if ( $p[1] != 'template_open' ) return ;
		$ret = array ( 'name' => '' , 'params' => array() ) ;
		if ( count($p[2]) == 0 ) return $ret ; // Paranoia
		
		$this->shiftNewlines ( $p[2] ) ;
		$ret['name'] = array_shift ( $p[2] ) ;
		$ret['name'] = ucfirst(trim(str_replace('_',' ',$ret['name'][0]))) ;
		if ( count($p[2]) > 0 and $p[2][0][1] == 'pipe' ) array_shift ( $p[2] ) ;
		$param_count = 1 ;
		$this->shiftNewlines ( $p[2] ) ;
		while ( count($p[2]) > 0 ) {
			$key = '' ;
			if ( count($p[2]) > 1 and $p[2][1][1] == 'eq' ) {
				$key = trim(str_replace('_',' ',$p[2][0][0])) ;
				array_shift ( $p[2] ) ;
				array_shift ( $p[2] ) ;
			} else {
				$key = $param_count++ ; // Increase on EVERY parameter, or on every non-key one?
				$key .= '' ;
			}
			$ret['params'][$key] = array() ;
			$this->shiftNewlines ( $p[2] ) ;
			while ( count($p[2]) > 0 and $p[2][0][1] != 'pipe' ) {
				$ret['params'][$key][] = array_shift ( $p[2] ) ;
				$this->shiftNewlines ( $p[2] ) ;
			}
			if ( count($p[2]) > 0 and $p[2][0][1] == 'pipe' ) {
				array_shift ( $p[2] ) ;
			}
			$this->shiftNewlines ( $p[2] ) ;
			if ( count($ret['params'][$key]) == 0 ) unset ( $ret['params'][$key] ) ;
		}
		
		$p[0] = $ret ;
		$p[1] = 'template' ;
		unset ( $p[2] ) ;
	}
	
	protected function removeNewlines ( &$parts ) {
		for ( $i = 0 ; $i < count($parts) ; $i++ ) {
			if ( !isset($parts[$i]) ) continue ;
			if ( !isset($parts[$i][1]) ) continue ;
			if ( $parts[$i][1] == 'template' ) {
				foreach ( $parts[$i][0]['params'] AS $k => $v ) {
					foreach ( $v AS $k0 => $v0 ) $this->removeNewlines ( $parts[$i][0]['params'][$k][$k0][0] ) ;
				}
			}
			if ( isset ( $parts[$i][2] ) ) $this->removeNewlines ( $parts[$i][2] ) ;
			if ( $parts[$i][1] == 'newline' ) {
				array_splice ( $parts , $i , 1 ) ;
				$i-- ;
			}
		}
	}
	
	protected function simplifyLinks ( &$p ) {
		if ( $p[1] != 'link_open' ) return ;
		if ( count($p[2]) == 0 ) return ;
		if ( preg_match ( '/^\s*[Cc]ategory\s*:\s*(.+)\s*$/' , $p[2][0][0] , $m ) ) {
			$p[1] = 'category' ;
			$p[0] = ucfirst(trim(str_replace('_',' ',$m[1]))) ;
			unset ( $p[2] ) ;
			return ;
		}
		if ( preg_match ( '/^\s*[Uu]ser\s*:\s*(.+)\s*$/' , $p[2][0][0] , $m ) ) {
			$p[1] = 'user_link' ;
			$p[0] = ucfirst(trim(str_replace('_',' ',$m[1]))) ;
			unset ( $p[2] ) ;
			return ;
		}
	}

	// "Tag templates" that can appear anywhere
	protected $tag_templates = array ( 'Commonist' , 'Personality rights' ) ;
	
	protected function removeTagTemplates ( &$p ) {
		if ( $p[1] != 'template' ) return ;
		$template = $p[0]['name'] ;
		if ( !in_array ( $template , $this->tag_templates ) ) return ;
		$this->json['tag_template'][] = array ( 'text' , $template ) ;
		$p = array () ;
	}

	protected function branchAllSubs ( &$parts , $func ) {
		$killme = array() ;
		for ( $i = 0 ; $i < count($parts) ; $i++ ) {
			if ( $parts[$i][1] == 'template' ) {
				foreach ( $parts[$i][0]['params'] AS $k => $v ) {
					$this->branchAllSubs ( $parts[$i][0]['params'][$k] , $func ) ;
				}
			}
			if ( isset($parts[$i][2]) ) {
				foreach ( $parts[$i][2] AS $k => $v ) $this->branchAllSubs ( $parts[$i][2] , $func ) ;
			}
			$this->$func ( $parts[$i] ) ;
			if ( count($parts[$i]) == 0 ) $killme[] = $i ;
		}
		while ( count($killme) > 0 ) array_splice ( $parts , array_pop($killme) , 1 ) ;
		if ( count($parts) == 0 ) $parts[] = array ( '' , 'text' ) ; // Paranoia
	}
	
	protected function getUserURL ( $user , $server = '' ) {
		if ( !isset($server) or $server == '' ) $server = 'commons.wikimedia.org' ;
		return "https://$server/wiki/User:" . myurlencode ( $user ) ;
	}
	
	protected function parts2text ( $part_list ) {
		if ( !isset($part_list) ) return '' ;
		if ( count($part_list) == 0 ) return '' ;
		
		if ( !isset($part_list) ) return '' ;
		if ( !is_array($part_list) ) return json_encode ( $part_list ) ;

		$ret = array() ;
		if (  count($part_list[0]) == 1 ) { // template list thing
			foreach ( $part_list AS $p ) $ret[] = $this->parts2text ( $p ) ;
		} else {
			foreach ( $part_list AS $p ) {
				if ( !is_array($p) or !isset($p[1]) ) $ret[] = json_encode($p) ;
				else if ( $p[1] == 'text' ) $ret[] = $p[0] ;
				else if ( isset ( $p[2] ) ) $ret[] = $this->parts2text ( $p[2] ) ;
				else if ( $p[1] == 'template' ) {
					$s = '{{' . $p[0]['name'] ;
					foreach ( $p[0]['params'] AS $k0 => $v0 ) {
						$s .= '|' ;
						if ( !preg_match ( '/^\d+$/' , $k0 ) ) $s .= $k0 . '=' ;
						$s .= $this->parts2text ( $v0 ) ;
					}
					$s .= '}}' ;
					$ret[] = $s ;
				} else if ( $p[1] == 'user_link' ) {
					$ret[] = '[' . $this->getUserURL ( $p[0] ) . ' ' . $p[0] . ']' ;
				} else { // TODO
					$ret[] = json_encode($p) ;
				}
			}
		}
		return implode ( ' ' , $ret ) ;
	}
	
	protected function resolveRedirect ( $page ) {
		$page = ucfirst ( trim ( str_replace ( ' ' , '_' , $page ) ) ) ;
		$ret = $page ;
		$j = json_decode ( $this->getURL ( "https://commons.wikimedia.org/w/api.php?action=query&redirects&format=json&titles=".myurlencode($page) ) ) ;
		foreach ( $j->query->pages AS $k => $v ) {
			$ret = $v->title ;
		}
		return $ret ;
	}
	
	protected function getLicenseItemForTemplate ( $template ) {
		$template = $this->resolveRedirect ( "Template:$template" ) ;
		$template = preg_replace ( '/^Template:/' , '' , str_replace ( '_' , ' ' , $template ) ) ;
		
		$db = openDB ( 'wikidata' , 'wikidata' ) ;
		$sql = "select ips_item_id from wb_items_per_site WHERE ips_site_id='commonswiki' AND ips_site_page='Template:".$db->real_escape_string($template)."'" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
		while($o = $result->fetch_object()){
			return $o->ips_item_id ;
		}
	}
	
	protected function partsAreComplex ( $part_list ) {
		if ( !isset($part_list) ) return false ;
		if ( count($part_list) == 0 ) return false ;
		
		if ( !isset($part_list) ) return false ;
		if ( !is_array($part_list) ) return false ;

		$ret = array() ;
		if ( count($part_list[0]) == 1 ) { // template list thing
			return $this->partsAreComplex ( $part_list[0] ) ;
		} else {
			foreach ( $part_list AS $p ) {
				if ( !is_array($p) ) continue ;
				if ( !isset($p[1]) ) continue ;
				if ( $p[1] == 'text' ) continue ;
				if ( $p[1] == 'template' ) return true ;
				if ( isset ( $p[2] ) ) {
					if ( $this->partsAreComplex ( $p[2] ) ) return true ;
				} else return true ;
			}
		}
		return false ;
	}
	
	protected function isLanguageTemplate ( $name ) { // Language template guess, TODO improve
		return preg_match ( '/^[A-Z][a-z]{1,2}$/' , $name ) ;
	}
	
	protected function isLicenseTemplate ( $name ) { // Guessing here...
		if ( preg_match ( '/^PD(|-.+)$/i' , $name ) ) return true ;
		if ( preg_match ( '/^CC-/i' , $name ) ) return true ;
		if ( preg_match ( '/^GFDL-/i' , $name ) ) return true ;
		return false ;
	}
	
	protected function parseDate ( $text ) {
		$ret = '' ;
		if ( preg_match ( '/^(\d{4}-\d\d-\d\d)\s+(\d\d:\d\d:\d\d)/' , $text , $m ) ) {
			$ret = array ( 'time' , "+".$m[1]."T".$m[2]."Z/14" ) ; // TODO CHECK PRECISION
		} else if ( preg_match ( '/^(\d{4}-\d\d-\d\d)/' , $text , $m ) ) {
			$ret = array ( 'time' , "+".$m[1]."T00:00:00Z/11" ) ;
		} else if ( preg_match ( '/^(\d{4}-\d\d)/' , $text , $m ) ) {
			$ret = array ( 'time' , "+".$m[1]."-01T00:00:00Z/10" ) ;
		} else if ( preg_match ( '/^(\d{4})/' , $text , $m ) ) {
			$ret = array ( 'time' , "+".$m[1]."-01-01T00:00:00Z/9" ) ;
		} else {
			$ret = array ( 'text' , $text ) ;
		}
		return $ret ;
	}
	
	protected $self_templates = array (
		'Own' ,
//		'Own assumed' ,
		'Self-photographed'
	) ;

	// ________________________________________________________________________________________________________________________________________________________________________________________________
	// BEGIN MAIN TEMPLATE SECTION
	// ________________________________________________________________________________________________________________________________________________________________________________________________
	
	
	protected function parseMainTemplateAuthor ( &$json , &$pparts , $key , $text , $is_complex , &$ii , &$uploader ) {
		if ( count($pparts) == 1 and count($pparts[0]) == 2 and $pparts[0][1] == 'user_link' ) {
			$json['author'][] = array ( 'url' , $this->getUserURL ( myurlencode($pparts[0][0]) ) ) ;
		} else if ( count($pparts) == 1 and count($pparts[0]) == 2 and $pparts[0][1] == 'template' and count($pparts[0][0]['params'])==0 and preg_match('/^User:([^\/]+)/',$pparts[0][0]['name'],$m) ) {
			$json['author'][] = array ( 'url' , $this->getUserURL($m[1]) ) ;
		} else if ( count($pparts) == 1 and count($pparts[0]) == 2 and $pparts[0][1] == 'template' and count($pparts[0][0]['params'])==3 and $pparts[0][0]['name']=='User at project' ) {
			$user = $pparts[0][0]['params'][1][0][0] ;
			$project = $pparts[0][0]['params'][2][0][0] ;
			$lang = $pparts[0][0]['params'][3][0][0] ;
			$json['author'][] = array ( 'url' , $this->getUserURL($user,"$lang.$project.org") ) ;
		} else if ( $is_complex ) {
			return $this->setError ( "Author parts are complex" , $pparts ) ;
		} else {
			$json['author'][] = array ( 'text' , $text ) ;
		}
	}

	protected function parseMainTemplateSource ( &$json , &$pparts , $key , $text , $is_complex , &$ii , &$uploader ) {
		if ( count($pparts) == 1 and count($pparts[0]) == 2 and $pparts[0][1] == 'template' and in_array($pparts[0][0]['name'],$this->self_templates) and count($pparts[0][0]['params'])==0 ) {
			$json['author'][] = $uploader ; // Self => uploader
		} else if ( preg_match ( '/^(http|https|ftp):\/\/\S+$/' , $text ) ) {
			$json['source'][] = array ( 'url' , $text ) ;
		} else if ( preg_match ( '/^\[(http|https|ftp)(:\/\/\S+)\]$/' , $text , $m ) ) {
			$json['source'][] = array ( 'url' , $m[1] ) ;
		} else if ( preg_match ( '/^\[(http|https|ftp)(:\/\/\S+) (.+?)\]$/' , $text , $m ) ) {
			$json['source'][] = array ( 'url' , $m[1].$m[2] ) ;
			$json['source'][] = array ( 'text' , $m[3] ) ;
		} else if ( $is_complex ) {
			return $this->setError ( "Source parts are complex" , $pparts ) ;
		} else {
			$json['source'][] = array ( 'text' , $text ) ;
		}
	}

	protected function parseMainTemplateDate ( &$json , &$pparts , $key , $text , $is_complex , &$ii , &$uploader ) {
		if ( count($pparts) == 1 and count($pparts[0]) == 2 and $pparts[0][1] == 'template' and $pparts[0][0]['name']=='Taken on' and count($pparts[0][0]['params'])==1 ) {
			$text = $pparts[0][0]['params'][1][0][0] ;
			$json['date'][] = $this->parseDate ( $text ) ;
/*		} else if ( count($pparts) == 1 and count($pparts[0]) == 2 and $pparts[0][1] == 'template' and $pparts[0][0]['name']=='According to EXIF data' and count($pparts[0][0]['params'])==1 ) {
			$text = $pparts[0][0]['params'][1][0][0] ;
			$json['date'][] = $this->parseDate ( $text ) ;*/
		} else if ( $is_complex ) {
			return $this->setError ( "Date parts are complex" , $pparts ) ;
		} else {
			$json['date'][] = $this->parseDate ( $text ) ;
		}
	}

	protected function parseMainTemplatePermission ( &$json , &$pparts , $key , $text , $is_complex , &$ii , &$uploader ) {
		foreach ( $pparts AS $k0 => $v0 ) {
			if ( $v0[1] == 'text' ) {
				$json['permission'][] = array ( 'monolingual:en' , $text ) ;
				continue ;
			}
//print_r ( $v0 ) ;
			if ( $this->partsAreComplex ( $v0 ) ) return $this->setError ( "Permission of complex elements not implemented" , $v0 ) ;
			$lang = 'en' ;
			$text = '' ;
			if ( $v0[1] == 'template' and $this->isLanguageTemplate ( $v0[0]['name'] ) and count($v0[0]['params']) == 1 ) {
				$lang = strtolower($v0[0]['name']) ;
				$text = trim ( $this->parts2text ( $v0[0]['params'][1] ) ) ;
			} else {
				$text = trim ( $this->parts2text ( $v0 ) ) ;
			}
			$json['permission'][] = array ( 'monolingual:'.$lang , $text ) ;
		}
	}

	protected function parseMainTemplateDescription ( &$json , &$pparts , $key , $text , $is_complex , &$ii , &$uploader ) {
		$trans = array () ;
		foreach ( $pparts AS $dummy0 => $v0 ) {
			if ( $v0[1] == 'template' and count($v0[0]['params']) == 1 and $this->isLanguageTemplate($v0[0]['name']) ) {
				foreach ( $v0[0]['params'] AS $v1 ) {
					$trans[strtolower($v0[0]['name'])][] = trim ( $this->parts2text ( $v1 ) ) ;
				}
			} else if ( $this->partsAreComplex ( $v0 ) ) {
				return $this->setError ( "Description parts are complex" , $pparts ) ;
			} else {
				$trans['en'][] = trim ( $this->parts2text ( $v0 ) ) ;
			}
		}
		foreach ( $trans AS $lang => $v ) {
			$v = trim ( implode ( ' ' , $v ) ) ;
			if ( $v == '' ) continue ;
			$json['description'][] = array ( 'monolingual:'.$lang , $v ) ;
		}
	}
	
	// ________________________________________________________________________________________________________________________________________________________________________________________________
	// END MAIN TEMPLATE SECTION
	// ________________________________________________________________________________________________________________________________________________________________________________________________
	
	protected function parseTemplateInformation ( &$v_main , &$json , &$ii , &$uploader ) {
		foreach ( $v_main[0]['params'] AS $key => $pparts ) {
			$key = strtolower(trim(str_replace('_',' ',$key))) ;
			$text = trim ( $this->parts2text ( $pparts ) ) ;
			$is_complex = $this->partsAreComplex ( $pparts ) ;
			
			$fn = 'parseMainTemplate' . ucfirst ( $key ) ;
			if ( method_exists ( $this , $fn ) ) {
				$this->$fn ( $json , $pparts , $key , $text , $is_complex , $ii , $uploader ) ;
			} else if ( $key == 'other versions' ) {
				return $this->setError ( "Other versions not implemented" , $pparts ) ;
			} else { // Unknown {{Information}} parameter
				return $this->setError ( "Unknown {{Information}} parameter $key" , $pparts ) ;
			}
		}
	}

	protected function parseTemplateObjectLocationDec ( &$v , &$json , &$ii , &$uploader ) {
		return $this->parseTemplateLocationDec ( $v , $json , $ii , $uploader ) ;
	}

	protected function parseTemplateLocationDec ( &$v , &$json , &$ii , &$uploader ) {
		$o = array () ;
		$o['latitude'] = $this->parts2text ( $v[0]['params']['1'] ) ;
		$o['longitude'] = $this->parts2text ( $v[0]['params']['2'] ) ;
		if ( isset ( $v[0]['params']['3'] ) ) $o['attributes'] = $this->parts2text ( $v[0]['params']['3'] ) ;
		if ( isset ( $v[0]['params']['wikidata'] ) ) $o['q'] = $this->parts2text ( $v[0]['params']['wikidata'] ) ;
		foreach ( array('bare','secondary','prec') AS $v0 ) {
			if ( !isset ( $v[0]['params'][$v0] ) ) continue ;
			$o[$v0] = $this->parts2text ( $v[0]['params'][$v0] ) ;
		}
		
		foreach ( $o AS $k => $v ) {
			if ( trim($v) == '' ) unset ( $o[$k] ) ;
		}
		$json['location'][] = array ( 'coordinate' , $o ) ;
	}

	protected function parseTemplateSelf ( &$v , &$json , &$ii , &$uploader ) {
		$json['author'][] = $uploader ; // Self => uploader
		foreach ( $v[0]['params'] AS $k => $param ) {
			if ( $k == 'author' ) {
				if ( $this->partsAreComplex ( $param ) ) return $this->setError ( "{{Self}}: author too complex" , $param ) ;
				$json['author'][] = array ( 'text' , $this->parts2text ( $param ) ) ;
				continue ;
			}
			$template = $this->parts2text ( $param ) ;
			$template = ucfirst ( trim ( str_replace ( '_' , ' ' , $template ) ) ) ;
			$q = $this->getLicenseItemForTemplate ( $template ) ;
			if ( !isset($q) ) return $this->setError ( "No item for template {{"."$template}}" , $v ) ;
			$json['license'][$template] = array ( 'url' , "https://www.wikidata.org/wiki/Q$q" ) ;
		}
	}

	protected function parseTemplateFlickrReview ( &$v , &$json , &$ii , &$uploader ) {
		if ( count($v[0]['params']) != 2 ) return $this->setError ( "Bad {{Flickrreview}}" , $v ) ;
		$o = array (
			'reviewer' => array ( 'url' , $this->getUserURL ( $v[0]['params'][1][0][0] ) ) ,
			'date' => $this->parseDate ( $v[0]['params'][2][0][0] )
		) ;
		$json['review'][] = $o ;
	}

	protected function parseTemplateOriginalDescriptionPage ( &$v , &$json , &$ii , &$uploader ) {
		if ( count($v[0]['params']) != 2 ) return $this->setError ( "Bad {{".$v[0]['name']."}}" , $v ) ;
		$wiki = $v[0]['params'][1][0][0] ;
		$page = urldecode ( $v[0]['params'][2][0][0] ) ;
		$o = array (
			array ( 'url' , 'https://'.$wiki.'.org/wiki/'.myurlencode($page) ) ,
			array ( 'text' , $v[0]['name'] )
		) ;
		$json['urls'][] = $o ;
	}
	
	
	protected function parseTemplateBotMoveToCommons ( &$v , &$json , &$ii , &$uploader ) {
		if ( count($v[0]['params']) != 4 ) return $this->setError ( "Bad {{".$v[0]['name']."}}" , $v ) ;
		$wiki = $v[0]['params'][1][0][0] ;
		$date = $v[0]['params']['year'][0][0] . '-' . $this->month2num($v[0]['params']['month'][0][0]) . '-' . $v[0]['params']['day'][0][0] ;
		$o = array (
			'source' => array ( 'url' , 'https://'.$wiki.'.org/' ) ,
			'date' => $this->parseDate ( $date )
		) ;
		$json['urls'][] = $o ;
	}
	
	protected function parseTemplateFlickreviewRpassed ( &$v , &$json , &$ii , &$uploader ) {
		if ( count($v[0]['params']) != 4 ) return $this->setError ( "Bad {{".$v[0]['name']."}}" , $v ) ;
		$photo_url = $v[0]['params'][2][0][0] ;
		if ( !preg_match ( '/^https{0,1}:\/\/flickr.com\/photos\/(.+?)\/(.+)$/' , $photo_url , $m ) ) return $this->setError ( "FlickrrevieweR: Not a photo URL: $photo_url" , $v ) ;
		$flickr_user = $m[1] ;
		$flickr_photo_id = $m[2] ; // Not used
		$flickr_user_label = $v[0]['params'][1][0][0] ;
		
		$flickr_url = array ( 'url' , $photo_url ) ;
		$review_date = $this->parseDate ( $v[0]['params'][3][0][0] ) ;
		$license = $this->getLicenseItemForTemplate ( $v[0]['params'][4][0][0] ) ;
		if ( !isset($license) ) return $this->setError ( "FlickrrevieweR: No Wikidata item for license template {{".$v[0]['params'][4][0][0]."}}" , $v ) ;
		$json['review'][] = array (
			'reviewer' => array ( 'url' , $this->getUserURL ( 'FlickreviewR' ) ) ,
			'date' => $review_date ,
			'license' => [ 'url' , 'https://www.wikidata.org/wiki/Q'.$license ]
		) ;

		$json['author'][] = array ( 'url' , 'https://www.flickr.com/photos/'.$flickr_user ) ;
		$json['author'][] = array ( 'text' , $flickr_user_label ) ;
		$json['source'][] = array ( 'url' , $photo_url ) ;
	}

	protected function parseTemplateGeograph ( &$v , &$json , &$ii , &$uploader ) {
		if ( count($v[0]['params']) != 2 ) return $this->setError ( "Bad {{".$v[0]['name']."}}" , $v ) ;
		$photo_id = $v[0]['params'][1][0][0] ;
		$photo_url = "http://www.geograph.org.uk/photo/$photo_id" ;
		$user_name = $v[0]['params'][2][0][0] ;
		$json['author'][] = array ( 'text' , "$user_name (at geograph.co.uk)" ) ;
		$json['source'][] = array ( 'url' , $photo_url ) ;
	}

	protected function parseSimpleTemplate ( &$v , &$json , &$ii , &$uploader , $template_name , $aux ) {
		$o = [
			'name' => $template_name ,
			'parameters' => []
		] ;
		foreach ( $v[0]['params'] AS $k2 => $v2 ) {
			$o['parameters'][$k2] = $v2[0][0] ;
		}
		$json['template'][] = $o ;
	}

	protected function logit ( $v ) {
		if ( $this->verbose ) {
			print_r ( $v ) ;
		}
		return false ;
	}
	
	protected function explainParts ( &$parts , $ii ) {
		// Init
		$templateParsingFunctions = array (
			'Information' => 'parseTemplateInformation' ,
			'Object location dec' => 'parseTemplateObjectLocationDec' ,
			'Location dec' => 'parseTemplateLocationDec' ,
			'Flickrreview' => 'parseTemplateFlickrReview' ,
			'Original description page' => 'parseTemplateOriginalDescriptionPage' ,
			'BotMoveToCommons' => 'parseTemplateBotMoveToCommons' ,
			'User:FlickreviewR/reviewed-pass' => 'parseTemplateFlickreviewRpassed' ,
			'Geograph' => 'parseTemplateGeograph' ,
			'Self' => 'parseTemplateSelf'
		) ;

		$simpleTemplates = [
			'Rijksmonument' => [] ,
			'Extracted from' => []
		] ;
	
		$this->json = array ( "categories"=>array() , 'author'=>array() , 'license'=>array() , 'uploader'=>array() ) ;
		$this->branchAllSubs ( $parts , "removeTagTemplates" ) ;
		$this->removeNewlines ( $parts ) ;
		$json = $this->json ;
		unset ( $this->json ) ;
		

		
		// Get image information from API data
		foreach ( $ii AS $k => $i ) {
			$a = array ( 'url' , $this->getUserURL ( $i->user ) ) ;
			if ( $k > 0 ) $a[] = 'deprecated' ;
			$json['uploader'][] = $a ;
		}
		$ii = $ii[0] ;
		$uploader = array ( 'url' , $this->getUserURL ( $ii->user ) ) ;
		
		// Explain top-level parts
		foreach ( $parts AS $k => $v ) {
#print_r ( $v ) ;
			if ( $v[1] == 'template' and isset($simpleTemplates[$v[0]['name']]) ) { // Templates with bespoke function

				$result = $this->parseSimpleTemplate ( $v , $json , $ii , $uploader , $v[0]['name'] , $simpleTemplates[$v[0]['name']] ) ;
				if ( $result === false ) return $this->logit($v) ;

			} else if ( $v[1] == 'template' and isset($templateParsingFunctions[$v[0]['name']]) ) { // Templates with bespoke function
				
				$fn = $templateParsingFunctions[$v[0]['name']] ;
				$result = $this->$fn ( $v , $json , $ii , $uploader ) ;
				if ( $result === false ) return $this->logit($v) ;

			} else if ( $v[1] == 'template' and $this->isLicenseTemplate($v[0]['name']) and count($v[0]['params'])==0 ) { // License template, no parameters

				$template = $v[0]['name'] ;
				$q = $this->getLicenseItemForTemplate ( $template ) ;
				if ( !isset($q) ) return $this->setError ( "No Wikidata item for template {{"."$template}}" , $v ) ;
				$json['license'][$template] = array ( 'url' , "https://www.wikidata.org/wiki/Q$q" ) ;

			} else if ( $v[1] == 'category' ) {
				$json['categories'][$v[0]] = $v[0] ;

			} else if ( $v[1] == 'text' and !$this->partsAreComplex ( $v ) ) { // Just some plain text on main level
				$text = trim ( $v[0] ) ;
				$trans = array ( "monolingual:en" , $text ) ;
				$json['description'][] = $trans ;

			} else if ( $v[1] == 'template' and $this->isLanguageTemplate($v[0]['name']) ) { // Getting more desparate; out-of-place language template fallback
			
				$lang = strtolower($v[0]['name']) ;
				$trans = array() ;
				foreach ( $v[0]['params'] AS $v1 ) {
					$trans = array ( "monolingual:$lang" , trim ( $this->parts2text ( $v1 ) ) ) ;
				}
				$json['description'][] = $trans ;

			} else if ( $v[1] == 'template' and count($v[0]['params'])==0 ) { // Unknown template, no parameters
			
				$template = $v[0]['name'] ;
				$json['tag_template'][] = array ( 'text' , $template ) ;
				
			} else if ( $v[1] == 'template' ) { // Unknown template fallback
				return $this->setError ( "Can not explain template {{".$v[0]['name']."}}" , $v ) ;

			} else {
				return $this->setError ( "FAILED TO EXPLAIN" , array ( "key"=>$k , "data"=>$v , "parts"=>$parts ) ) ;
			}
		}
		
		// Collapse multiple identical entries
		foreach ( array('license','categories','author','uploader','tag_template') AS $type ) {
			if ( !isset($json[$type]) ) continue ;
			$json[$type] = array_values ( $json[$type] ) ;
			foreach ( $json[$type] AS $k => $v ) $json[$type][$k] = json_encode($v) ;
			$json[$type] = array_unique ( $json[$type] ) ;
			foreach ( $json[$type] AS $k => $v ) $json[$type][$k] = json_decode($v) ;
		}
		
		// Merge descriptions per language
		if ( isset ( $json['description'] ) ) {
			$tmp = array() ;
			foreach ( $json['description'] AS $v ) {
				if ( isset($tmp[$v[0]]) and $tmp[$v[0]] != '' ) {
					$tmp[$v[0]] .= "; " . trim($v[1]) ;
				} else {
					$tmp[$v[0]] = trim($v[1]) ;
				}
			}
			$json['description'] = array() ;
			foreach ( $tmp AS $k => $v ) $json['description'][] = array ( $k , $v ) ;
		}
		
		return $json ;
	}

	
	public function parseFileDescription ( $file ) {
		$url = 'https://commons.wikimedia.org/w/index.php?title=File:'.myurlencode($file).'&action=raw' ;
		$wikitext = $this->getURL ( $url ) ;
		
		$url = "https://commons.wikimedia.org/w/api.php?action=query&titles=File:".myurlencode($file)."&prop=imageinfo&iiprop=user|size|mediatype|metadata|commonmetadata&format=json&iilimit=500" ;
		$tmp = json_decode ( $this->getURL ( $url ) ) ;
		foreach ( $tmp->query->pages AS $k => $v ) $ii = $v->imageinfo ;

		// Remove comments
		if ( 1 ) { # Do remove comments
			$wikitext = preg_replace ( '|<!--.*?-->|' , '' , $wikitext ) ;
		}
		
		// Remove headings
		$rows = explode ( "\n" , $wikitext ) ;
		$wikitext = array() ;
		foreach ( $rows AS $r ) {
			if ( preg_match ( '/^\s*={2,}.+={2,}\s*$/' , $r ) ) continue ;
			$wikitext[] = $r ;
		}
		$wikitext = trim ( implode ( "\n" , $wikitext ) ) ;

		$parts = $this->lexer ( $wikitext , $this->wikiRules ) ;
		if ( $parts === false ) return $this->logit("No results from lexer") ;
		
		$this->subgroupParts ( $parts , '{{' , '}}' ) ; // Group templates
		$this->subgroupParts ( $parts , '[[' , ']]' ) ; // Group links
		
		$this->branchAllSubs ( $parts , "parseTemplate" ) ;
		$this->removeNewlines ( $parts ) ;
		$this->branchAllSubs ( $parts , "simplifyLinks" ) ;

		$json = $this->explainParts ( $parts , $ii ) ;
		return $json ;
	}



/*
	// DATABASE UPDATES, MAY BE OBSOLETE

	protected function getDB () {
		if ( isset($this->db) and $this->db->ping() ) return $this->db ;
		$this->db = openToolDB ( 'commonsedge_p' ) ;
		$this->db->set_charset("utf8") ;
		return $this->db ;
	}

	protected function get_db_safe ( $s , $fixup = false ) {
		if ( $fixup ) $s = str_replace ( ' ' , '_' , trim ( ucfirst ( $s ) ) ) ;
		return $this->db->real_escape_string ( $s ) ;
	}

	protected function make_db_safe ( &$s , $fixup = false ) {
		$s = $this->get_db_safe ( $s , $fixup ) ;
	}
	

	// Adds new files to the tool DB
	public function updateFiles () {
		$min_time = '' ;
		$sql = "SELECT max(page_touched) AS t FROM file" ;
		if(!$result = $this->db->query($sql)) return $this->setError('updateFiles[1] : There was an error running the query [' . $this->db->error . "]\n$sql\n");
		while($o = $result->fetch_object()) $min_time = $o->t ;
		
		$dbc = openDB ( 'commons' , 'commons' ) ;
		$sql = "SELECT * FROM page WHERE page_namespace=6 AND page_touched>='$min_time' AND page_is_redirect=0" ;
		if(!$result = $dbc->query($sql)) return $this->setError('updateFiles[2] : There was an error running the query [' . $dbc->error . "]\n$sql\n");
		$this->getDB() ; // Ensure connection
		while($o = $result->fetch_object()) {
			$title = $this->db->real_escape_string($o->page_title) ;
			$sql = "INSERT INTO file (page_id,page_latest,page_touched,title,status,json,parser_version) VALUES " ;
			$sql .= "({$o->page_id},{$o->page_latest},'{$o->page_touched}','$title',0,'',{$this->parser_version})" ;
			$sql .= " ON DUPLICATE KEY UPDATE page_touched='{$o->page_touched}',page_latest={$o->page_latest},title='$title',status=0,json=''" ;
			if(!$result2 = $this->db->query($sql)) return $this->setError('updateFiles[3] : There was an error running the query [' . $this->db->error . "]\n$sql\n");
		}
		$dbc->close() ;
	}
*/


} ;

?>